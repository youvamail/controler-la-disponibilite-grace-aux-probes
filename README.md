# Contrôler la disponibilité grâce aux Probes



## Liste des fichiers
Ce dépôt contient les différents fichiers utilisés pour illustrer le fonctionnement des Probes:
- liveness-pod.yaml : LivenessProbe utilisant une requête HTTP
- liveness-pod2.yaml : LivenessProbe utilisant une commande Shell
- readiness-pod.yaml : ReadinessProbe